![amelifr-redesign_pp.png](./amelifr-redesign_pp.png)

# Redesign low-tech du site ameli.fr

## Contexte

Ce projet a été réalisé dans le cadre d'un cours de "web design" de deuxième année de formation d'[ingénieur IMAC](https://www.ingenieur-imac.fr/), sous la supervision de Fabien Huet.
La consigne était de choisir un site web ou une application mobile et d'en refaire le design en changeant un paramètre : la cible, le contenu, l'utilisation ...

## Choix du sujet

Je suis très intéressé par toutes les questions d'écologie et sobriété, notamment numérique. J'ai donc choisi de redesigner le [site de l’Assurance Maladie (ameli.fr)](https://www.ameli.fr/) de façon “low-tech”, c’est à dire en visant une interface qui consomme le moins de ressources possible (mémoire, puissance de calcul, bande-passante ...), qui est utilisable avec une connexion faible et/ou instable et sur des terminaux peu puissants, tout en gardant un maximum des fonctionnalités. L’objectif n’est pas de faire le site le plus léger possible, mais bien de trouver le meilleur équilibre entre tous les points évoqués précédemment.

Dans une “vraie” démarche low-tech, le design arriverait plutôt vers la fin, après qu’on ait (re)penser tout ce qu’il y a en amont (l’objectif du site et du service, le backend …). Ici je ferai abstraction de ces aspects, en cherchant à maintenir les fonctionnalités principales du site dans une interface plus légère.

### Pourquoi https://www.ameli.fr/ ?

ce site, étant un service public national lié à la santé, a tout à fait lieu d’exister dans un monde où l’on essaie de réduire notre consommation de ressources
le site actuel me semble excessivement lourd par rapport à son design et ses fonctionnalités
on retrouve beaucoup de chose différentes dans le design de ce site : images, menus, éléments dynamiques, textes informatif … ce qui rend le travail de redesign plus intéressant

## Benchmark

### Tour d'horizon de sites web "low-tech".

- https://solar.lowtechmagazine.com/ : blog entièrement pensé de façon “low-tech”
    - polices par défaut uniquement
    - travail sur les tailles, positions et couleurs des textes qui donnent une hiérarchie visuelle claire et agréable
    - images monochromatiques en 6 niveaux de couleurs, donc très légères
    - très peu d’animations, et très simples, mais suffisantes pour que l’utilisateur se repère facilement
    - responsif malgré tout
- https://www.thegreenroom.fr/
    - Site extrêmement léger (page d’accueil <100ko) tout en étant joli, agréable à lire et responsif
    - Images vectorielle
- https://www.pikselkraft.com/en/ (agence qui a fait le site thegreenroom.fr)
- https://lowtechlab.org/fr : version classique de https://solar.lowtechmagazine.com/
- https://small-tech.org/#site-js
    - créatif sur les couleurs et les formes
    - peu d’images mais textes bien organisés et hiérarchisés, ce qui donne un ensemble agréable à lire
- https://bare.eco/
    - site extrêmement léger (~10ko/page)
    - très lisible
    - aucune image
    - extrêmement minimaliste

### Conclusion du benchmark

- Les images sont des éléments lourds donc on les utilise avec parcimonie, en essayant de les alléger le plus possible. Pour cela plusieurs méthodes ressortent particulièrement :
    - utiliser des images vectorielles
    - utiliser des images monochromes avec un filtre CSS
    - utiliser des images tramées
Dans l’idéal, **les images ne doivent pas être des éléments essentiels de la page**, ne pas être requises pour sa compréhension ni pour naviguer dedans.
- **Limiter les animations** à celles qui servent vraiment la compréhension de l’interface, et les garder très simples.
- Puisqu’on limite les images et les animations, **on mise beaucoup sur le texte**, qui constitue le coeur de la page et de l’information. Il doit impérativement être :
    - lisible
    - bien hiérarchisé visuellement
    - bien écrit (travail important sur le wording ...)
- Un des objectif est que le site soit accessible depuis des terminaux très peu puissants et/ou anciens, donc avec des dimensions d’écrans et des résolutions très variées. Il est donc impératif que le site soit **parfaitement responsif** sur toutes les tailles d’écran, y celles qui sortent des standards actuels.

**De manière générale, l'objectif est la sobriété. On se concentre sur l’intérêt premier du site et de la page, avec le plus de simplicité et de clarté possible, et on enlève tous les éléments superflus.**

## Moodboard

Disponible ici : https://miro.com/app/board/o9J_lRy8Itw=/

## Persona

Personnage fictif représentant la cible d'un produit (voir la [définition wikipédia](https://fr.wikipedia.org/wiki/Persona)).

Isabelle Duchêne
- Française, 38 ans
- Mariée, mère de 2 enfants de 7 et 13 ans
- Professeure des écoles
- Habite un petit village de campagne où la connexion internet n’est pas toujours bonne
- Habitudes :
    - Maîtrise basique d’internet des nouvelle technologies
    - Sensible aux questions écologiques, elle préfère toujours faire de la récupération ou réparation de son matériel. Ainsi, elle utilise un ordinateur portable moyen de gamme datant de 2012, et un smartphone de 2016
- Besoin :
    - elle souhaite pouvoir accéder facilement et rapidement à tous les détails de son compte ameli et de ceux de ses enfants et de son mari, depuis n’importe quel périphérique, et en toutes circonstances
- Ce qu'elle aime sur un site :
    - trouver rapidement ce qu'elle cherche
    - qu'il soit agréable à lire
    - pouvoir y accéder depuis son smartphone
- Ce qu'elle n'aime pas sur un site
    - qu’il mette trop de temps à charger
    - qu’il lague
    - devoir chercher longtemps dans les menus ou dans les pages pour trouver ce qui l’intéresse
    - être submergé d’infos, popup, publicité ou autres éléments qu’elle juge inutiles

## Zoning

Disponible ici : https://whimsical.com/zoning-webdesign-imac2-Ct8b3wuHGwWV9iSwc35UVw

## Wireframe, Design system et Maquette :

Disponibles ici : https://www.figma.com/file/UQSQiGzMoQnOe2BMW4kFOE/IMAC2_WebDesign_ameli.fr?node-id=101%3A1379

> Utiliser le bouton en haut à gauche pour accéder aux différentes parties ![navigation_figma.png](./navigation_figma.png)
